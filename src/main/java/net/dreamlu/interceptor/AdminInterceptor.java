package net.dreamlu.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

import net.dreamlu.common.WebUtils;
import net.dreamlu.model.User;
import net.dreamlu.vo.AjaxResult;

/**
 * 后台拦截
 * @author L.cm
 * @date 2013-5-30 下午9:52:46
 */
public class AdminInterceptor implements Interceptor {
	
	@Override
	public void intercept(Invocation inv) {
		Controller controller = inv.getController();
		User user = WebUtils.currentUser(controller);
		if(null == user){
			controller.redirect("/sign_in");
			return;
		}
		// 方法名
		String methodName = inv.getMethodName();
		// 用户的权限进行控制
		Integer authority = user.getAuthority();
		// 普通用户没有 save update 和 delete 权限
		AjaxResult ajax = new AjaxResult(); 
		if (User.V_P == authority && (methodName.startsWith("save") || methodName.startsWith("delete"))) {
			ajax.addError("你没有权限操作，请联系管理员！");
			controller.renderJson(ajax);
		} else if (User.V_V == authority && methodName.startsWith("delete")) {
			ajax.addError("你没有权限操作，请联系管理员！");
			controller.renderJson(ajax);
		} else {
			inv.invoke();
		}
	}
}
