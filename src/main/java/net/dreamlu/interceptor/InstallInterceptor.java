package net.dreamlu.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.kit.StrKit;

import net.dreamlu.common.Consts;
import net.dreamlu.model.Options;
import net.dreamlu.model.User;

/**
 * 安装拦截
 * @author L.cm
 * @date Nov 2, 2013 2:05:00 PM
 */
public class InstallInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation inv) {
        Controller controller = inv.getController();
        if (!Consts.IS_INSTALL) {
            // 1.用户跳转到安装页
            long userCount = User.dao.findCounts();
            if (userCount < 1) {
                String ctxPath = JFinal.me().getContextPath();
                Options options = Options.dao.findByCache();
                if (StrKit.notBlank(ctxPath) && StrKit.isBlank(options.getCdnPath())) {
                    options.setCdnPath(ctxPath);
                    options.update();
                }
                inv.getController().setAttr("options", options);
                controller.render("/admin/install.vm");
                return;
            } else {
                Consts.IS_INSTALL = true;
            }
        }
        inv.invoke();
    }

}
