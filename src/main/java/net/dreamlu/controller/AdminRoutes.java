package net.dreamlu.controller;

import com.jfinal.config.Routes;
import com.jfinal.weixin.demo.WeixinMsgController;

import net.dreamlu.controller.admin.*;

/**
 * 后台路由
 * @author L.cm
 * email: 596392912@qq.com
 * site:http://www.dreamlu.net
 * date: 2016年1月14日 下午10:19:06
 */
public class AdminRoutes extends Routes {

	@Override
	public void config() {
		add("/admin", AdminController.class);
		add("/admin/blog", AdminBlogController.class);
		add("/admin/links", AdminLinksController.class);
		add("/admin/options", AdminOptionsController.class);
		add("/admin/tags", AdminTagsController.class);
		add("/admin/user", AdminUserController.class);
		add("/admin/wechat", AdminWeChatController.class);

		add("/ueditor/api", UeditorApiController.class);
		add("/weixin/msg", WeixinMsgController.class);
	}

}
