package net.dreamlu.controller.admin;

import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;

import net.dreamlu.ext.base.BaseController;
import net.dreamlu.interceptor.AdminInterceptor;
import net.dreamlu.model.Links;
import net.dreamlu.vo.AjaxResult;

/**
 * 网站设置
 * @author L.cm
 * @date 2013-6-4 上午10:16:14
 */
@Before(AdminInterceptor.class)
public class AdminLinksController extends BaseController {

	/**
	 * 后台 链接 列表
	 * @return void	返回类型
	 * @throws
	 */
	public void list_json() {
		int iDisplayStart = getParaToInt("iDisplayStart", 0);
		int pageSize = getParaToInt("iDisplayLength", 10);
		int pageNum =  iDisplayStart / pageSize + 1;

		String search = getPara("sSearch");
		renderDataTable(Links.dao.pageDataTables(pageNum, pageSize, search));
	}

	/**
	 * 后台写博或跟新
	 * @return void	返回类型
	 * @throws
	 */
	public void add_edit() {
		Integer id = getParaToInt();
		if(null != id){
			Links links = Links.dao.findById(id);
			setAttr("links", links);
		}
	}

	/**
	 * 跟新网站友链
	 * @Title: save_update
	 * @return void	返回类型
	 * @throws
	 */
	public void save_update(){
		Links links = getModel(Links.class);
		boolean state = false;
		if(null == links.getId()){
			state = links.save();
		} else {
			state = links.update();
		}
		renderJson(new AjaxResult(state));
	}
	
	/**
	 * 删除或显示博文
	 * @return void	返回类型
	 * @throws
	 */
	public void delete_show() {
		Links links = Links.dao.findById(getParaToInt());
		boolean temp = false;
		if(StrKit.notNull(links)){
			int status = links.getDelStatus();
			links.setDelStatus(status == 1 ? 0 : 1);
			temp = links.update();
		}
		renderJson(new AjaxResult(temp));
	}
}
