package net.dreamlu.controller.admin;

import com.jfinal.aop.Before;
import com.jfinal.json.Json;
import com.jfinal.kit.HashKit;
import com.jfinal.kit.StrKit;
import net.dreamlu.common.Consts;
import net.dreamlu.common.WebUtils;
import net.dreamlu.ext.base.BaseController;
import net.dreamlu.interceptor.AdminInterceptor;
import net.dreamlu.model.*;
import net.dreamlu.validator.EmailValidator;
import net.dreamlu.validator.PwdValidator;
import net.dreamlu.vo.AjaxResult;

import java.util.List;

/**
 * 网站后台
 * @author L.cm
 * @date 2013-6-1 下午5:29:58
 */
public class AdminController extends BaseController {

	/**
	 * 后台首页,博文热度图
	 */
	@Before(AdminInterceptor.class)
	public void index() {
		List<List<Integer>> hostBlogList = Blog.dao.hostBlogList();
		setAttr("data",  Json.getJson().toJson(hostBlogList));
	}

	/**
	 * 安装
	 */
	@Before({EmailValidator.class, PwdValidator.class})
	public void install() {
		String email = getPara("email");
		String pwd = getPara("pwd");
		
		User user = new User();
		user.setEmail(email);
		user.setNickName(email.substring(0, email.indexOf("@")));
		user.setPassword(WebUtils.pwdEncode(pwd));
		// avatar 头像
		String avatarUrl = "";//Options.dao.loadOrPut("avatar_url");
		user.setHeadPhoto(avatarUrl + HashKit.md5(email.trim()));
		user.setAuthority(User.V_A); // 设置成admin
		boolean temp = User.dao.save();
		if (temp) {
			Consts.IS_INSTALL = true;
		}
		renderJson(new AjaxResult(temp));
	}
	
	/**
	 * 登录
	 */
	@Before({EmailValidator.class, PwdValidator.class})
	public void session() {
		String email = getPara("email");
		String pwd = getPara("pwd");
		String remember = getPara("remember", "0");
		System.out.println(WebUtils.pwdEncode(pwd));
		User user = User.dao.login(email, WebUtils.pwdEncode(pwd));
		
		boolean temp = false;
		if(StrKit.notNull(user)) {
			temp = user.update();
			WebUtils.loginUser(this, user, remember.equals("1"));
		}
		renderJson(new AjaxResult(temp));
	}
	
	/**
	 * 用户绑定
	 */
	@Before({EmailValidator.class})
	public void binding() {
		String email = getPara("email");
		int wbId = getParaToInt("id");
		WbLogin wb = WbLogin.dao.findById(wbId);
		if (null == wb) {
			renderJson(new AjaxResult(false));
			return;
		}
		User user = User.dao.findByEmail(email);
		if (null == user) {
			user = new User();
			user.setEmail(email);
			user.setNickName(email.substring(0, email.indexOf("@")));
			user.setAuthority(User.V_V); // 设置成admin
			user.save();
		}
		wb.setUserId(user.getId());
		boolean status = wb.update();
		if (status){
			MailVerify verify = new MailVerify();
			// 设置成WbLogin
			verify.setUserId(wb.getUserId());
			status = verify.save();
			// 邮件校验
//			Map<String, Object> model = new HashMap<String, Object>();
//			Options options = Options.dao.findByCache();
//			model.put("options", options);
//			model.put("verifyUrl",  "/finish?code=" + verify.getStr(MailVerify.VERIFY_CODE));
//			// options.SITE_URL, options.wb_qq, options.wb_sina
//			Mail.sendTemplateMail("DreamLu博客邮箱绑定", email, model, "signup_send.html");
		}
		renderJson(new AjaxResult(status));
	}
}
