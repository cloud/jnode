package net.dreamlu.controller.admin;

import com.jfinal.aop.Before;
import net.dreamlu.ext.base.BaseController;
import net.dreamlu.interceptor.AdminInterceptor;
import net.dreamlu.model.BlogTag;
import net.dreamlu.model.Tags;
import net.dreamlu.vo.AjaxResult;

import java.util.List;

/**
 * 标签管理
 * @author L.cm
 * @date 2013-5-29 下午11:31:43
 */
@Before(AdminInterceptor.class)
public class AdminTagsController extends BaseController {

	/**
	 * 后台 链接 列表
	 * @return void	返回类型
	 * @throws
	 */
	public void list_json() {
		int iDisplayStart = getParaToInt("iDisplayStart", 0);
		int pageSize = getParaToInt("iDisplayLength", 10);
		int pageNum =  iDisplayStart / pageSize + 1;

		String search = getPara("sSearch");
		renderDataTable(Tags.dao.pageDataTables(pageNum, pageSize, search));
	}

	/**
	 * 所有标签 select2 ?? textext 插件?
	 * @return void	返回类型
	 * @throws
	 */
	public void all() {
		String query = getPara("query");
		int count	= getParaToInt("count", 5);
		List<Tags> tagsList = Tags.dao.findByNameCount(query, count);
		renderJson(tagsList);
	}
	
	/**
	 * 添加标签
	 * @return void	返回类型
	 * @throws
	 */
	public void add_tags() {
		String tagName = getPara("tagName");
		Tags tags = Tags.dao.findByName(tagName);
//		if (null == tags) {
//			tags = new Tags();
//			tags.set(Tags.TAG_NAME, tagName).save();
//			setAttr("tagsid", tags.get(Tags.ID));
//			setAttr(Consts.AJAX_STATUS, Consts.AJAX_Y);
//		} else {
//			setAttr(Consts.AJAX_STATUS, Consts.AJAX_N);
//		}
//		renderJson(new String[]{"tagsid", Consts.AJAX_STATUS});
	}
	
	/**
	 * 删除或显示博文
	 * @return void	返回类型
	 * @throws
	 */
	public void delete() {
		int tagid = getParaToInt();
		boolean temp = Tags.dao.deleteById(getParaToInt());
		if (temp) {
			BlogTag.dao.deleteByTagId(tagid);
		}
		renderJson(new AjaxResult(temp));
	}
}
