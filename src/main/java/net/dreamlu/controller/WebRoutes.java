package net.dreamlu.controller;

import com.jfinal.config.Routes;

import net.dreamlu.controller.web.BlogController;
import net.dreamlu.controller.web.IndexController;
import net.dreamlu.controller.web.LifeController;
import net.dreamlu.controller.web.QrcodeController;

/**
 * 前台页面路由
 * @author L.cm
 * email: 596392912@qq.com
 * site:http://www.dreamlu.net
 * date: 2016年1月14日 下午10:16:31
 */
public class WebRoutes extends Routes {

	@Override
	public void config() {
		add("/", IndexController.class, "web");
		add("/blog", BlogController.class, "web");
		add("/api/qrcode", QrcodeController.class);
		add("/life/itunes", LifeController.class, "web/life");
	}

}
