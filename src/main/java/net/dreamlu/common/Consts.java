package net.dreamlu.common;

/**
 * 系统常量
 * @author L.cm
 * email: 596392912@qq.com
 * site:  http://www.dreamlu.net
 * @date 2013-7-1 上午9:28:16
 */
public class Consts {
	
	/**
	 * 缓存枚举
	 * @author L.cm
	 */
	public static enum CacheName {
		session {
			@Override
			public String get() {
				return this.name();
			}
		}, 
		halfHour {
			@Override
			public String get() {
				return this.name();
			}
		},
		hour {
			@Override
			public String get() {
				return this.name();
			}
		},
		oneDay {
			@Override
			public String get() {
				return this.name();
			}
		},
		_default {
			@Override
			public String get() {
				return this.name().substring(1);
			}
		};
		
		public abstract String get();
	}
	
	// 安装状态
	public static boolean IS_INSTALL = false;
	// 分页大小
	public static final int BLOG_PAGE_SIZE = 8;
	
}
