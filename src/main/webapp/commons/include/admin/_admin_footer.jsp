<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<footer>
    <hr/>
    <p class="pull-right">
        GitHub <a href="${options.git_url}" target="_blank">${options.site_name}</a>by
        <a href="${options.git_url}" target="_blank">${options.site_name}</a>
    </p>
    <p>
        © 2013 <a href="${options.git_url}" target="_blank">${options.site_name}</a>
    </p>
</footer>