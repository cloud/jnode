<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<article class="article">
    <div id="content_box">
        <div id="post-356" class="post-356 post type-post status-publish format-standard hentry category-featured category-wellhead tag-social-media-2 g post cat-32-id cat-80-id has_thumb">
            <div class="single_post">
                <div class="post-date-ribbon">
                    <div class="corner"></div>${x:format(blog.update_time, "yyyy年 MM月 dd日") }
                </div>
                <div class="breadcrumb">
                    <a href="${ctxPath}/" rel="nofollow">首页</a>&nbsp;&nbsp;»&nbsp;&nbsp;
                    <a href="${ctxPath}${typeUrl}" title="${typeName}">${typeName}</a>&nbsp;&nbsp;»&nbsp;&nbsp; ${blog.title}
                </div>
                <header>
                    <h1 class="title single-title">${blog.title}</h1>
                        <span class="theauthor single-postmeta">
                            <div class="author_mt hp_meta">
                                <span class="mt_icon"></span>
                                <c:if test="${blog.url ne null }">
                                    <a href="${blog.url}" target="_blank" title="作者：${blog.nick_name}">${blog.nick_name}</a>
                                </c:if>
                                <c:if test="${blog.url eq null }">
                                    ${blog.nick_name}
                                </c:if>
                            </div>
                        <div class="cat_mt hp_meta">${tags}</div>
                        <div class="time_mt hp_meta">
                            <span class="time_icon"> </span>${x:format(blog.create_time, "yyyy-MM-dd") }
                        </div>
                        <div class="view_mt hp_meta">
                            <span class="view_icon"> </span>${blog.view_count}
                        </div>
                        <div class="comment_mt hp_meta">
                            <a href="#commentsAdd" title="评论数">
                                <span class="mt_icon"></span>
                                <span data-thread-key="${blog.id}" data-count-type="comments" class="ds-thread-count"></span>
                            </a>
                        </div>
                    </span>
                </header>
                <!-- .headline_area-->
                <div class="post-single-content box mark-links">${blog.content}</div>
            </div>
            <div class="post-lisence">
                <div class="ds-share flat" data-thread-key="${blog.id}" 
                        data-title="${blog.title}" data-images="${x:getBlogImg(blog.content)}" 
                        data-content="${x:filterSubText(blog.content, 30)}" 
                        data-url="${options.site_url}/blog/${blog.id}">
                    <div class="ds-share-inline">
                        <ul class="ds-share-icons-16">
                            <li data-toggle="ds-share-icons-more"><span class="ds-more">分享到：</span></li>
                            <li><a class="ds-weibo" href="javascript:void(0);" data-service="weibo">微博</a></li>
                            <li><a class="ds-qzone" href="javascript:void(0);" data-service="qzone">QQ空间</a></li>
                            <li><a class="ds-qqt" href="javascript:void(0);" data-service="qqt">腾讯微博</a></li>
                            <li><a id="ds_wechat" class="ds-wechat" href="javascript:void(0);" data-service="wechat">微信</a></li>
                        </ul>
                        <div class="ds-share-icons-more"></div>
                    </div>
                </div>
                <div id="ds-alipay">
                    <img height="80" width="80" src="${options.cdn_path}/images/alipay.png?v=${options.site_version}" alt="捐助共勉" title="捐助共勉">
                </div>
                版权声明：若无特殊注明，本文皆为原创，转载请保留文章出处。<br/>
            </div>
            <div class="postauthor">
                <c:if test="${blog.blog_type eq 0 } ">
                    <h4>关于作者</h4>
                    <img alt="${blog.nick_name}" src="${blog.head_photo}" height="80" width="80" style="border-radius: 40px;" class="avatar avatar-85 photo"/>
                    <h5>
                        <c:if test="${blog.url ne null }">
                            <a href="${blog.url}" target="_blank" title="作者：${blog.nick_name}">${blog.nick_name}</a>
                        </c:if>
                        <c:if test="${blog.url eq null }">
                            ${blog.nick_name}
                        </c:if>
                    </h5>
                    <p>${blog.signature}</p>
                </c:if>
                <c:if test="${blog.blog_type ne 0 } ">
                    <h4>原文地址：</h4><a href="${blog.share_url}" target="_blank">${blog.share_url}</a>
                </c:if>
            </div>
            <div class="related-posts">
                <script type="text/javascript" id="wumiiRelatedItems"></script>
            </div>
        </div><!-- post-->
        <div id="commentsAdd">
            <!-- duoshuo  评论-->
            <div data-thread-key="${blog.id}" data-title="${blog.title}" data-category="snode" data-author-key="${blog.user_id}" class="ds-thread"></div>
        </div>
    </div>
</article>