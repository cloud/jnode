<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<header class="main-header">
    <div class="container">
        <div id="header">
            <h1 id="logo">
                <a href="${ctxPath}">Dr<span>ea</span>mLu</a>
            </h1>
            <!-- END #logo-->
            <div class="widget-header">
                <div class="banner">
                    <div class="textwidget">
                        <img width="728" height="70" alt="${options.site_name}" src="${options.cdn_path}/static/images/banner.png?v=${options.site_version}"/>
                    </div>
                </div>
            </div>
        </div>
        <!-- #header-->
        <div class="secondary-navigation">
            <nav id="navigation">
                <ul id="menu-main-navigation" class="menu sf-js-enabled">
                    <li id="menu-item-1305" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1305"><a href="${ctxPath}/">首页</a></li>
                    <li id="menu-item-1286" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1286"><a href="${ctxPath}/blogs">原创 • 文章</a></li>
                    <li id="menu-item-1287" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1287"><a href="${ctxPath}/favorites">收藏 • 文章</a></li>
                    <li id="menu-item-1298" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1298"><a class="sf-with-ul">生活 • 娱乐</a>
                        <ul class="sub-menu sf-js-enabled">
                            <li id="menu-item-1303" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1303"><a href="${ctxPath}/life/itunes">itunes排行</a></li>
                            <li id="menu-item-1303" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1303"><a href="http://movie.dreamlu.net" target="_blank">电影资源</a></li>
                        </ul>
                    </li>
                    <li id="menu-item-1290" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1290"><a href="${ctxPath}/plugin">JFinal插件库</a></li>
                    <li id="menu-item-1290" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1290"><a href="http://www.dreamlu.net">如梦技术</a></li>
                    <li id="menu-item-1290" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-1290"><a href="${ctxPath}/about">关于</a></li>
                </ul>
            </nav>
        </div>
    </div><!-- .container-->
</header>