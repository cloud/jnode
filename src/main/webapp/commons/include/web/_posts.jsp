<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<c:forEach var="item" items="${blogPage.list }">
    <div class="post excerpt">
        <div class="post-date-ribbon">
            <div class="corner"></div>${x:format(item.update_time, "yyyy年 MM月 dd日") }
        </div>
        <header>
            <h2 class="title">
                <a href="${ctxPath}/blog/${item.id}" rel="bookmark">${x:markKeywords(item.title, 50, keywords)}</a>
            </h2>
            <div class="post-info">
            <div class="author_mt hp_meta"><span class="mt_icon"></span>
                <c:if test="${item.url ne null }">
                    <a href="${item.url}" target="_blank" title="作者：${item.nick_name}">${item.nick_name}</a>
                </c:if>
                <c:if test="${item.url eq null }">
                    ${item.nick_name}
                </c:if>
            </div>
            <div class="cat_mt hp_meta">${x:bTags(item.id)}</div>
            <div class="time_mt hp_meta"><span class="time_icon"></span>${x:format(item.create_time, "yyyy-MM-dd")}</div>
            <div class="view_mt hp_meta"><span class="view_icon"></span>${item.view_count}</div>
            <div class="comment_mt hp_meta">
                <a href="${ctxPath}/blog/${item.id}#commentsAdd" title="评论数">
                    <span class="mt_icon"></span>
                    <span data-thread-key="${item.id}" data-count-type="comments" class="ds-thread-count"></span>
                </a>
            </div>
            </div>
        </header>
        <div class="post-content image-caption-format-1">${x:markKeywords(item.content, 497, keywords)}</div>
        <div class="readMore">
            <a href="${ctxPath}/blog/${item.id}" title="${item.title}" rel="more">阅读全部</a>
        </div>
    </div>
</c:forEach>