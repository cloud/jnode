<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%
// 如下参数需要在 include 该页面的地方被赋值才能使用，以下是示例
/*  
	<c:set var="currentPage" value="${blogPage.pageNumber}" />
	<c:set var="totalPage" value="${blogPage.totalPage}" />
	<c:set var="actionUrl" value="/blog/" />
	<c:set var="urlParas" value="" />
*/
%>

<c:if test="${urlParas == null}">
	<c:set var="urlParas" value="" />
</c:if>
<c:if test="${(totalPage > 0) && (currentPage <= totalPage)}">
	<c:set var="startPage" value="${currentPage - 4}" />
	<c:if test="${startPage < 1}" >
		<c:set var="startPage" value="1" />
	</c:if>
	<c:set var="endPage" value="${currentPage + 4}" />
	<c:if test="${endPage > totalPage}" >
		<c:set var="endPage" value="totalPage" />
	</c:if>
	
	<div class="pagination">
		<ul>
		<c:if test="${currentPage <= 4}">
			<c:set var="startPage" value="1" />
		</c:if>
		<c:if test="${(totalPage - currentPage) < 4}">
			<c:set var="endPage" value="${totalPage}" />
		</c:if>
		<li>
			<c:choose>
				<c:when test="${currentPage == 1}">
					<span class="currenttext">&larr; 上一页</span>
				</c:when>
				<c:otherwise>
					<a href="${actionUrl}${currentPage - 1}${urlParas}" class="prev_page">&larr; 上一页</a>
				</c:otherwise>
			</c:choose>
		</li>
		<c:if test="${currentPage > 4}">
			<li>
				<a href="${actionUrl}1${urlParas}">1</a>
			</li>
			<li>
				<span class="currenttext">…</span>
			</li>
		</c:if>
		<c:forEach begin="${startPage}" end="${endPage}" var="i">
			<li>
				<c:choose>
					<c:when test="${currentPage == i}">
						<span class="currenttext">${i}</span>
					</c:when>
					<c:otherwise>
						<a href="${actionUrl}${i}${urlParas}">${i}</a>
					</c:otherwise>
				</c:choose>
			</li>
		</c:forEach>
		<c:if test="${(totalPage - currentPage) >= 4}">
			<li>
				<span class="currenttext">…</span>
			</li>
			<li>
				<a href="${actionUrl}${totalPage}${urlParas}">${totalPage}</a>
			</li>
		</c:if>
		<li>
			<c:choose>
				<c:when test="${currentPage == totalPage}">
					<span class="currenttext">下一页 &rarr;</span>
				</c:when>
				<c:otherwise>
					<a href="${actionUrl}${currentPage + 1}${urlParas}">下一页 &rarr;</a>
				</c:otherwise>
			</c:choose>
		</li>
		</ul>
	</div>
</c:if>