<%@ page language="java" session="false" contentType="text/html;charset=UTF-8" isELIgnored="false"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ include file="/commons/global.jsp" %>
<%-- 布局 --%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <title>${options.siteName}管理后台</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <assets:assets var="href" file="/assets/assets-admin.jcss">
        <link rel="stylesheet" type="text/css" href="${options.cdnPath}${href}">
    </assets:assets>
    <link rel="stylesheet" type="text/css" href="http://cdn.staticfile.org/font-awesome/3.2.1/css/font-awesome.min.css"/>
    <!--[if lt IE 9]>
    <script src="http://cdn.staticfile.org/html5shiv/3.7/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="http://cdn.staticfile.org/font-awesome/3.2.1/css/font-awesome-ie7.min.css"/>
    <![endif]-->
    <%--重写的head --%>
    <layout:block name="head"></layout:block>
</head>
<!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
<!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
<!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
<!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<body>
<!--<![endif]-->
    <%--主体部分 --%>
    <layout:block name="content"></layout:block>
    <%--脚本部分--%>
    <script type="text/javascript">
        function getCtxPath() {
            return "${ctxPath}";
        }
    </script>
    <assets:assets var="x" file="/assets/assets-admin.jjs">
        <script src="${options.cdnPath}${x}" type="text/javascript" ></script>
    </assets:assets>
    <layout:block name="script"></layout:block>
</body>
</html>