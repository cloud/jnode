<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN" class="no-js">
    <head>
        <meta charset="UTF-8">
        <link rel="icon" type="image/x-icon" href="${options.cdnPath}/favicon.ico">
        <meta content="width=device-width,user-scalable=no" name="viewport">
        <meta content="IE=EmulateIE8" http-equiv="X-UA-Compatible">
        <meta content="yes" name="apple-mobile-web-app-capable">
        <meta content="black" name="apple-mobile-web-app-status-bar-style">
        <meta content="okapdtIdGB" name="baidu-site-verification">
        <assets:assets var="href" file="/assets/assets-web.jcss">
            <link rel="stylesheet" type="text/css" href="${options.cdnPath}${href}" media="all">
        </assets:assets>
        <!--[if lt IE 9]><script src="http://cdn.staticfile.org/html5shiv/3.7/html5shiv.min.js"></script><![endif]-->
        <link title="${options.siteName} Rss订阅" rel="alternate" type="application/rss+xml" href="${ctxPath }/rss.xml">
        <meta content="87ae82d9-c019-45d5-9f60-f46c407693b5" name="wumiiVerification">
        <meta name="baidu-site-verification" content="5Ps9CJAL2p" />
        <%--重写的head --%>
        <layout:block name="head"></layout:block>
    </head>
    <body id="blog" class="home blog main cat-32-id cat-80-id">
        <!--[if lte IE 7]><div class="old-ie">您的浏览器版本<strong>很旧很旧</strong>，为了正常地访问网站，请升级您的浏览器 <a target="_blank" href="http://browsehappy.com">立即升级</a></div><![endif]-->
        <div id="line"></div>
        <%@ include file="/commons/include/web/_header.jsp"%>
        <div class="main-container">
            <div id="page">
                <div class="content">
                    <%--主体部分 --%>
                    <layout:block name="content"></layout:block>
                </div>
            </div>
        </div>
        <%@ include file="/commons/include/web/_footer.jsp" %>
    <script type="text/javascript" src="${options.cdnPath}/static/js/lazyload-min.js"></script>
    <script type="text/javascript">
        var cdn_path = '${options.cdnPath}'; // 供js中使用
        var qrcode = '${qrcode}';
        var js = [
            <assets:assets var="src" file="/assets/assets-web.jjs">
            "${options.cdnPath}${src}"
            </assets:assets>
        ];
    </script>
    <%--脚本部分--%>
    <layout:block name="script"></layout:block>
    <script type="text/javascript">
        LazyLoad.js(js);
    </script>
    <div style="display:none;">
        <script src="http://s13.cnzz.com/stat.php?id=5631992&web_id=5631992"></script>
    </div>
</html>