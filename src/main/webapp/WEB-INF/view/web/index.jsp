<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%-- 填充head --%>
<layout:override name="head">
    <title>DreamLu开源博客 - JFinal,nodejs博客dreamlu.net</title>
    <meta content="dreamlu,jfinal,bae,开源博客" name="Keywords">
    <meta content="dreamlu.net 是一个使用Java开发，采用Jfinal mvc框架，MySQL数据库和Jade模版引擎，搭建于百度云BAE的开源博客系统。" name="Description">
</layout:override>

<%-- 填充content --%>
<layout:override name="content">
    <article class="article">
        <div id="content_box" class="home_page">
            <c:if test="${postsby eq null }">
                <h1 class="postsby">
                    <span>${postsby}</span>
                </h1>
            </c:if>
            <%@ include file="/commons/include/web/_posts.jsp"%>
            <c:set var="currentPage" value="${blogPage.pageNumber}" />
            <c:set var="totalPage" value="${blogPage.totalPage}" />
            <c:set var="urlParas" value="" />
            <%@ include file="/commons/include/web/_pagination.jsp"%>
        </div>
    </article>
    <aside class="sidebar c-4-12">
        <div id="sidebars" class="g">
            <div class="sidebar">
                <%@ include file="/commons/include/web/_sidebar.jsp"%>
            </div>
        </div>
    </aside>
</layout:override>

<%-- 填充script --%>
<layout:override name="script">
    <script type="text/javascript">
        var duoshuoQuery = {short_name:"${options.duoshuo_domain}"};
        js.push('http://static.duoshuo.com/embed.js');
    </script>
</layout:override>

<%@ include file="/commons/_layout_web.jsp" %>