<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%-- 填充head --%>
<layout:override name="head">
    <title>${blog.title} - JFinal,nodejs博客dreamlu.net</title>
    <c:if test="${keyWords eq null }">
        <c:set var="keyWords" value="dreamlu,jfinal,bae,开源博客"></c:set>
    </c:if>
    <meta name="Keywords" content="${keyWords}"/>
    <meta name="Description" content="${x:description(blog.content)}"/>
</layout:override>

<%-- 填充content --%>
<layout:override name="content">
    <%@ include file="/commons/include/web/_post.jsp"%>
    <aside class="sidebar c-4-12">
        <div id="sidebars" class="g">
            <div class="sidebar">
                <%@ include file="/commons/include/web/_sidebar.jsp"%>
            </div>
        </div>
    </aside>
</layout:override>

<%-- 填充script --%>
<layout:override name="script">
    <script type="text/javascript">
        // duoshuo config
        var duoshuoQuery = {short_name:"${options.duoshuo_domain}"};
        // wumii config
        var wumiiPermaLink = "${options.site_url}/blog/${blog.id}"; //请用代码生成文章永久的链接
        var wumiiTitle = "${blog.title}"; //请用代码生成文章标题
        var wumiiTags = "jfinal, nodejs, coffee-script, java, mysql, linux"; //请用代码生成文章标签，以英文逗号分隔，如："标签1,标签2"
        var wumiiSitePrefix = "${options.site_url}";
        var wumiiParams = "&num=5&mode=2&pf=Jnode";
        js.push("http://static.duoshuo.com/embed.js");
        js.push("http://widget.wumii.com/ext/relatedItemsWidget");
        js.push("${options.cdn_path}/SyntaxHighlighter/scripts/shCore.js")
        js.push("${options.cdn_path}/SyntaxHighlighter/scripts/shAutoloader.js")
        js.push("${options.cdn_path}/js/blog.js?v=${options.site_version}")
        LazyLoad.css("${options.cdn_path}/ueditor/third-party/SyntaxHighlighter/shCoreDefault.css");
    </script>
</layout:override>

<%@ include file="/commons/_layout_web.jsp" %>