<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%-- 填充head --%>
<layout:override name="head">
    <style>
        ul,li{list-style-type:none;}
        span.tag{display:inline-block;background:#FFF;border:1px solid #e6e6e6;padding:0 8px;height:24px;line-height:24px;max-width:160px;overflow:hidden;color:#666;}
        span.tag:hover{border-color:#BBB;color:#999;text-decoration:none;}
        a.icon-remove{vertical-align:top;margin-left:4px;}
        .attached{display:inline;}
        #addtags{border-radius:3px;margin-left:5px;margin-bottom:8px;}
    </style>
</layout:override>

<%-- 填充content --%>
<layout:override name="content">
    <c:if test="${blog.id eq null}">
        <c:set var="tab" value="修改博文"></c:set>
    </c:if>
    <c:if test="${blog.id ne null}">
        <c:set var="tab" value="写博"></c:set>
    </c:if>
    <c:set var="dashboardClasses" value="in"></c:set>
    <%@ include file="/commons/include/admin/_admin_navbar.jsp" %>
    <div class="content">
        <div class="header">
            <h1 class="page-title">${tab}</h1>
        </div>
        <ul class="breadcrumb">
            <li>
                <a href="/admin">首页</a><span class="divider">/</span>
            </li>
            <li class="active">${tab}</li>
        </ul>
        <div class="container-fluid">
            <div class="row-fluid">
                <form action="${ctxPath}/admin/blog/save_update" method="post">
                    <input type="hidden" name="blog.id" value="${blog.id}"/>
                    <div class="btn-toolbar">
                        <button type="submit" class="btn btn-primary">
                            <i class="icon-save"> </i>&nbsp;保存
                        </button><a href="${ctxPath}/admin/blog/list" class="btn">返回</a>
                        <div class="btn-group"></div>
                    </div>
                    <div class="well">
                        <div id="myTabContent" class="tab-content">
                            <label>标题</label>
                            <input type="text" name="blog.title" value="${blog.title}" required="required" class="input-xxlarge"/>
                            <label>分类</label>
                            <select id="DropDownTimezone" name="blog.blog_type" class="input-xlarge">
                                <c:if test="${blog.blog_type eq 0}">
                                    <option value="0" selected="selected">原创</option>
                                    <option value="1">收藏</option>
                                </c:if>
                                <c:if test="${blog.blog_type eq 1}">
                                    <option value="0">原创</option>
                                    <option value="1" selected="selected">收藏</option>
                                </c:if>
                            </select>
                            <label>标签</label>
                            <ul class="tag-list">
                                <c:forEach var="item" items="${tags}">
                                    <li class="attached">
                                        <span class="tag">${item.tag_name}</span>
                                        <a class="icon-remove" title="删除" href="#"></a>
                                        <input name="tags" value="${item.id}" type="hidden" />
                                    </li>
                                </c:forEach>
                            </ul>
                            <input id="autocomplete" type="text" class="input-xlarge"/>
                            <a id="addtags" class="btn btn-primary"><i class="icon-tag"></i>添加标签</a>
                            <label>来源</label>
                            <input type="text" name="blog.share_url" value="${blog.share_url}" class="input-xxlarge"/>
                            <label>内容</label>
                            <script id="container" name="blog.content" type="text/plain">
                                ${blog.content}
                            </script>
                        </div>
                    </div>
                </form>
            </div>
            <%@ include file="/commons/include/admin/_admin_footer.jsp" %>
        </div>
    </div>
</layout:override>

<%-- 填充script --%>
<layout:override name="script">
    <!-- ueditor-->
    <script type="text/javascript" charset="utf-8" src="${ctxPath}/static/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="${options.cdn_path}/static/ueditor/ueditor.all.js"></script>
    <script type="text/javascript">
        $(function(){
            // ueditor
            var editor = UE.getEditor('container');
            // 标签处理
            $('#autocomplete').autocomplete({
                source:function(query, callback){
                    var count = this.options.items;
                    $.get("${ctxPath}/admin/tags/all", {"query" : query, "count" : count}, function(data){
                        return callback(data);
                    });
                },
                formatItem:function(item){
                    return item.tag_name;
                },
                setValue:function(item){
                    return {'data-value': item.tag_name, 'data-id': item.id};
                }
            });
            // 删除标签
            $(document).on("click", ".icon-remove", function(e){
                $(this).parents('.attached').remove();
                return false;
            })
            // form 提交
            $('form').submit(function(){
                editor.sync();
                var dialog = $.dialog();
                var title = $('input[name="blog.title"]').val();
                var content = editor.getContent();
                if( title.length < 3 ){
                    dialog.content('标题太短！').time(1000);
                    return false;
                }
                if( content.length < 10 ){
                    dialog.content('亲再多些点吧！').time(1000);
                    return false;
                }
                _post(this, dialog, function(data){
                    if(data.code === 0){
                        dialog.content('保存成功！').lock().time(1000);
                        setTimeout(function(){
                            location.href = '${ctxPath}/admin/blog/list';
                        }, 1500);
                    }else{
                        dialog.content('服务器忙，请稍候！').lock().time(2000);
                    }
                });
                return false;
            });
            $('#addtags').click(function(){
                var tagname = $('#autocomplete').val();
                $.post('${ctxPath}/admin/tags/add_tags',{tagName: tagname}, function(data){
                    if (data.code === 0) {
                        $(".tag-list").append('<li class="attached"><span class="tag">' + tagname + '</span><a class="icon-remove" title="删除" href="#"></a><input name="tags" value="' + data.tagsid + '" type="hidden"></li>')
                    } else {
                        dialog.content('该标签已经存在，不能重复添加！').lock().time(2000);
                    }
                });
            });
        });
    </script>
</layout:override>

<%@ include file="/commons/_layout_admin.jsp" %>