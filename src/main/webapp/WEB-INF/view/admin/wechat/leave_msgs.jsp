<%@ page language="java" contentType="text/html;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<%-- 填充head --%>
<layout:override name="head">
    <style>
        .dataTables_length select { width: 80px;}
        .fg-toolbar { height: 30px;}
    </style>
</layout:override>

<%-- 填充content --%>
<layout:override name="content">
    <c:set var="wechatClasses" value="in"></c:set>
    <%@ include file="/commons/include/admin/_admin_navbar.jsp" %>
    <div class="content">
        <div class="header">
            <h1 class="page-title">微信管理</h1>
        </div>
        <ul class="breadcrumb">
            <li>
                <a href="${ctxPath}/admin">首页</a><span class="divider">/</span>
            </li>
            <li class="active">留言列表</li>
        </ul>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="well">
                    <table id="otable" cellpadding="0" cellspacing="0" border="0" width="100%" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="5%">id</th>
                            <th width="20%">用户</th>
                            <th width="65%">留言</th>
                            <th width="10%">操作</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <%@ include file="/commons/include/admin/_admin_footer.jsp" %>
        </div>
    </div>
</layout:override>

<%-- 填充script --%>
<layout:override name="script">
    <script id="manageTemp" type="text/html">
        <a href="${ctxPath}/admin/wechat/delete_msg/{{=id}}>" class="delete_show"><i class="icon-remove"></i>删除</a>
    </script>
    <script type="text/javascript" src="${options.cdn_path}/static/js/admin/wechat_msg_list.js"></script>
</layout:override>

<%@ include file="/commons/_layout_admin.jsp" %>