<%@ page language="java" contentType="text/xml;charset=UTF-8"%>
<%@ include file="/commons/global.jsp" %>
<?xml version="1.0" encoding="utf-8" ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <db:sqls var="x" sqlKey="findAll">
        <url>
            <loc>http://www.dreamlu.net/blog/${x.id}</loc>
            <priority>0.9</priority>
            <lastmod>${x:format(x.update_time, "yyyy-MM-dd") }</lastmod>
            <changefreq>weekly</changefreq>
        </url>
    </db:sqls>
</urlset>